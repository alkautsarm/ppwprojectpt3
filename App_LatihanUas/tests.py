from django.test import TestCase
from .models import Keranjang

# Create your tests here.
class AppLatihanUas(TestCase) :

	def test_page_exist(self):
		response = self.client.get('/latihan-uas')
		response2 = self.client.get('/add-item')
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response2.status_code, 302)

	def test_models(self):
		keranjang_new = Keranjang.objects.create(barang="test")
		collect = Keranjang.objects.all().count()
		self.assertEqual(collect, 1)

