from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Keranjang

def index(request):
	response = {'item':Keranjang.objects.all().values()}

	return render(request, 'belanja.html', response)

def add_item(request):
	if(request.POST) :
		Keranjang.objects.create(barang = request.POST['barang'])
		Keranjang.save()
		
	return HttpResponseRedirect('/latihan-uas')