from django.conf.urls import url
from .views import index, add_item

app_name = 'prc'

urlpatterns = [
	url(r'^latihan-uas$', index, name='index'),
	url(r'^add-item$', add_item, name='add-item'),
]