from django.urls import path

from . import views

urlpatterns = [
	path('profile', views.profilePage, name='profile_page'), #alamat di web, fungsi yg diakses, namespace(semacam var akses)
	path('profile/check/email/is_taken', views.emailValidation, name='email_validation'),
	path('profile/create/new_user', views.createUser, name='create_user'),
	path('profile/get/subscriber/subscriber_list', views.subscriberList, name='subscriber_list'),
]