from django.test import TestCase
from django.http import HttpRequest

from .views import profilePage

from .forms import RegistrationForm
from .models import RegistrationModels

# Create your tests here.
class ProfilePageTest(TestCase) :
	
	def test_profile_page_exist(self) :
		response = self.client.get('/profile')
		response1 = self.client.get('/profile/check/email/is_taken')
		response2 = self.client.get('/profile/get/subscriber/subscriber_list')
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response1.status_code, 200)
		self.assertEqual(response2.status_code, 200)

	def test_profile_page_using_templates(self) :
		response = self.client.get('/profile')
		self.assertTemplateUsed(response, 'profile_page.html')

	def test_profile_page_html_correct(self) :
		request = HttpRequest()
		response = profilePage(request)
		html = response.content.decode('utf8')
		self.assertTrue(html.startswith('<!DOCTYPE html>'))
		self.assertIn('<title>Profile</title>', html)
		self.assertIn('Muhammad Al-Kautsar Maktub', html)
		self.assertIn('1706043885', html)
		self.assertIn('18 Januari 1999', html)
		self.assertIn('Universitas Indonesia', html)
		self.assertTrue(html.endswith('</html>'))
	
	def test_form_exist_in_page(self):
		response = self.client.get('/profile')
		self.assertIsInstance(response.context['form'], RegistrationForm)

	def test_form_spesification(self):
		form = RegistrationForm()
		self.assertIn('placeholder="Tuliskan nama..."', form.as_p())
		self.assertIn('placeholder="Tuliskan email..."', form.as_p())
		self.assertIn('placeholder="Tuliskan password..."', form.as_p())
		self.assertIn('maxlength="35"', RegistrationForm()['namaForm'].as_widget())
		self.assertIn('maxlength="100"', RegistrationForm()['emailForm'].as_widget())
		self.assertIn('minlength="8"', RegistrationForm()['passwordForm'].as_widget())
		self.assertIn('maxlength="35"', RegistrationForm()['passwordForm'].as_widget())

	def test_form_model_works(self):
		new_user = RegistrationModels.objects.create(nama='Ferguso', email='ferguso@contoh.com', password='12345678')
		user_total = RegistrationModels.objects.all().count()
		self.assertEqual(user_total, 1)
