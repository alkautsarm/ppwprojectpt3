from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

from .forms import RegistrationForm
from .models import RegistrationModels

import json

mhs_name = 'Muhammad Al-Kautsar Maktub' 
birth_date = "18 Januari 1999"
npm = 1706043885 
tempat_kuliah = 'Universitas Indonesia'
hobi = 'Mempelajari hal-hal yang baru'
deskripsi = 'Saya berkuliah di UI. Saya orang yang suka mempelajari \
hal-hal baru.'

def profilePage(request) :
	response = {'name': mhs_name,  'tgl_lahir':birth_date, 'npm': npm, 
	'tempatkuliah' : tempat_kuliah, 'hobi' : hobi, 'deskripsi' : deskripsi, 'form':RegistrationForm()}

	return render(request, 'profile_page.html', response)

def emailValidation(request) :
	emailFilled = request.GET.get('emailForm', None)
	data = {
		'is_taken':RegistrationModels.objects.filter(email = emailFilled).exists()
	}
	return JsonResponse(data)

def createUser(request) :
	registrationForm =  RegistrationForm(request.POST or None)
	condition = {'condition':'Success'}
	
	if(request.method == 'POST' and registrationForm.is_valid()) :
		nama_user = request.POST['namaForm']
		email_user = request.POST['emailForm']
		password_user = request.POST['passwordForm']

		registrationModel = RegistrationModels(nama = nama_user, email = email_user,
			password = password_user)
		registrationModel.save()
	else  :
		if (list(registrationForm.errors.as_data().keys())[0] == 'emailForm') : #kl sy test ini dgn akses page(status code 200), error krn [0]nya blm ada tpi kl udh jd web sabi
			condition['condition'] = 'Failed. Please enter correct email.'
		elif (list(registrationForm.errors.as_data().keys())[0] == 'passwordForm') :
			condition['condition'] = "Failed. Please make sure total password's characters not more than 35 and not less than 8."
		
	return JsonResponse(condition)

def subscriberList(request) :
	allSubscriber = RegistrationModels.objects.all().values()
	subscriberList = []
	data = {'data' : []}

	if(request.method == 'POST') :
		emailFilled = request.POST['email']
		RegistrationModels.objects.filter(email = emailFilled).delete()

	else :	
		for x in range (len(allSubscriber)) :
			subscriberList.append({'nama':allSubscriber[x]['nama'], 'email':allSubscriber[x]['email']})

		data['data'] = subscriberList
	return JsonResponse(data)

