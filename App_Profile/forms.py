from django import forms

class RegistrationForm(forms.Form) :
	namaForm = forms.CharField(label="Nama", max_length=35,
		widget = forms.TextInput(attrs={'placeholder':'Tuliskan nama...', 'class':'form-control'}))
	emailForm = forms.EmailField(label="Email", max_length=100,
		widget = forms.EmailInput(attrs={'placeholder':'Tuliskan email...', 'class':'form-control'}))
	passwordForm = forms.CharField(label="Password", min_length=8, max_length=35,
		widget = forms.PasswordInput(attrs={'placeholder':'Tuliskan password...', 'class':'form-control'}))
