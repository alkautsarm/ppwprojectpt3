from django.shortcuts import render
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status

def landingPage(request) :
	statusForm = StatusForm(request.POST or None)
	list_status = Status.objects.all().values()
	if(request.method == 'POST' and statusForm.is_valid()) :
		status_text = statusForm.cleaned_data['statusField']
		status_construct = Status(status = status_text)
		status_construct.save()
	return render(request, 'landing_page.html', {'form' : StatusForm(), 'display':list_status})