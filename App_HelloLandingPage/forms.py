from django import forms

class StatusForm(forms.Form) :
	statusField = forms.CharField(label = '', max_length = 300, 
		widget=forms.TextInput(attrs={'placeholder':'Tulis status...'}))