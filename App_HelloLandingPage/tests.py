from django.test import TestCase
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

from .views import landingPage
from .forms import StatusForm
from .models import Status

# Create your tests here.
'''
class HelloLandingPageFunctionalTest(TestCase) :
	
	def setUp(self) :
		options = Options()
		options.add_argument('--headless')
		options.add_argument('disable-gpu')
		options.add_argument('--dns-prefetcsh-disable')
		options.add_argument('--no-sandbox')

		service_log_path = "./chromedriver.log"

		service_args = ['--verbose']
		
		self.browser = webdriver.Chrome('.\\chromedriver.exe', chrome_options=options)

	def tearDown(self) :
		time.sleep(3) #tahan dulu agar tdk lgsg ke step selanjutnya
		self.browser.quit()

	def test_this_page_input_and_submit(self) :
		#Si A mendengar ada website landing page yang bisa dia akses
		self.browser.get('http://localhost:8000')
		time.sleep(3) #tahan dulu agar tdk lgsg ke step selanjutnya

		#Si A melihat ada tulisan "Hello, Apa Kabar?" yang cukup besar di page tsb dan title yang sama
		self.assertIn('Hello, Apa Kabar?', self.browser.title)

		header = self.browser.find_element_by_id('id_helloDisplay')
		self.assertEqual('0px', header.value_of_css_property('margin-top'))
		self.assertIn('Hello, Apa Kabar?', header.text)

		#Si A juga melihat sebuah teks box (form)tempat menuliskan status hari ini (max 300 char)
		status_box = self.browser.find_element_by_id('id_statusField')

		#Si A mencoba menulis status yang dia inginkan
		status_siA = 'Coba-Coba'
		status_box.send_keys(status_siA)

		#Si A bisa melakukan submit dari status yang ia tulis untuk disimpan di database
		submit_button = self.browser.find_element_by_id('id_statusSubmit')
		submit_button.send_keys(Keys.RETURN)
			#submit_button.submit() #bisa juga pake ini

		#Ternyata Si A juga bisa melihat daftar dari status yang sudah ia buat
		list_status = self.browser.find_elements_by_id('id_statusDaftar')
		self.assertIn(status_siA, [element.text for element in list_status])
		
		#Si A melihat background berwarna hijau
		body = self.browser.find_element_by_tag_name('body')
		background_color = body.value_of_css_property('background-color')
		self.assertEqual('rgba(19, 206, 102, 1)', background_color)
		self.assertEqual('0px', body.value_of_css_property('margin'))

		#Si A melihat button untuk mengubah tema
		themeButton1 = self.browser.find_element_by_id('id_ubahTema1')
		themeButton2 = self.browser.find_element_by_id('id_ubahTema2')

		#Si A mencoba mengklik tombol warna yang berbeda dan tampilan berubah
		body = self.browser.find_element_by_tag_name('body')
		background_color = body.value_of_css_property('background-color')
		themeButton2.send_keys(Keys.RETURN)
		background_color2 = body.value_of_css_property('background-color')
		self.assertFalse(background_color == background_color2)

		#Si A mencoba mengklik tombol hijau dan background jadi hijau
		body = self.browser.find_element_by_tag_name('body')
		background_color = body.value_of_css_property('background-color')
		themeButton1.send_keys(Keys.RETURN)
		background_color2 = body.value_of_css_property('background-color')
		self.assertFalse(background_color == background_color2)
'''

class HelloLandingPageUnitTest(TestCase) :
	
	def test_landing_page_exist(self) :
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_landing_page_using_templates(self) :
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'landing_page.html')

	def test_landing_page_html_correct(self) :
		request = HttpRequest()
		response = landingPage(request)
		html = response.content.decode('utf8')
		self.assertTrue(html.startswith('<!DOCTYPE html>'))
		self.assertIn('<title>Hello, Apa Kabar?</title>', html)
		self.assertIn('Hello, Apa Kabar?', html)
		self.assertTrue(html.endswith('</html>'))

	def test_status_forms(self) :
		form = StatusForm()		
		self.assertIn('placeholder="Tulis status..."', form.as_p())
		self.assertIn('maxlength="300"', form.as_p())

	def test_landing_page_uses_forms(self) :
		response = self.client.get('/')
		self.assertIsInstance(response.context['form'], StatusForm)

	def test_models(self) :
		new_status = Status.objects.create(status='Hari ini aku lab')
		collect = Status.objects.all().count()
		self.assertEqual(collect, 1)