from django.urls import path

from . import views

urlpatterns = [
	path('bookshelf', views.bookshelfPage, name='bookshelf_page'), #alamat di web, fungsi yg diakses, namespace(semacam var akses)
	path('bookshelf/api/book', views.create_json, name='bookshelf_api_json'),
	path('bookshelf/api/book1', views.search_book1, name='bookshelf_api_json1'),
	path('bookshelf/api/book2', views.search_book2, name='bookshelf_api_json2'),
	path('bookshelf/api/book3', views.search_book3, name='bookshelf_api_json3'),
]