from django.shortcuts import render

from django.http import HttpResponse

import json, urllib.request

# Create your views here.
def bookshelfPage(request) :
	return render(request, 'app_bookshelf.html')

def create_json (request) :
	url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
	jsonresponse = urllib.request.urlopen(url)
	data = json.loads(jsonresponse.read())

	data_json = json.dumps({'data':data['items']})
	return HttpResponse(data_json)

def search_book1(request) :
	url = 'https://www.googleapis.com/books/v1/volumes?q=action'
	jsonresponse = urllib.request.urlopen(url)
	data = json.loads(jsonresponse.read())

	data_json = json.dumps({'data':data['items']})
	return HttpResponse(data_json)

def search_book2(request) :
	url = 'https://www.googleapis.com/books/v1/volumes?q=comic'
	jsonresponse = urllib.request.urlopen(url)
	data = json.loads(jsonresponse.read())

	data_json = json.dumps({'data':data['items']})
	return HttpResponse(data_json)

def search_book3(request) :
	url = 'https://www.googleapis.com/books/v1/volumes?q=design'
	jsonresponse = urllib.request.urlopen(url)
	data = json.loads(jsonresponse.read())

	data_json = json.dumps({'data':data['items']})
	return HttpResponse(data_json)