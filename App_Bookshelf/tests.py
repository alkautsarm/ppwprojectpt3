from django.test import TestCase

from selenium import webdriver

import urllib.request

# Create your tests here.
class AppBookshelfUnitTest(TestCase) :

	def test_web_page_exist(self) :
		response = self.client.get('/bookshelf')
		response1 = self.client.get('/bookshelf/api/book')
		response2 = self.client.get('/bookshelf/api/book1')
		response3 = self.client.get('/bookshelf/api/book2')
		response4 = self.client.get('/bookshelf/api/book3')

		self.assertEqual(response.status_code, 200)
		self.assertEqual(response1.status_code, 200)
		self.assertEqual(response2.status_code, 200)
		self.assertEqual(response3.status_code, 200)
		self.assertEqual(response4.status_code, 200)

	def test_bookshelf_page_using_templates(self) :
		response = self.client.get('/bookshelf')
		self.assertTemplateUsed(response, 'app_bookshelf.html')

		##kemungkinan tes nya tntg ngecek web jsonnya ada atau engga

	def test_api_page_exist(self) :
		jsonresponse1 = urllib.request.urlopen('https://www.googleapis.com/books/v1/volumes?q=quilting')
		jsonresponse2 = urllib.request.urlopen('https://www.googleapis.com/books/v1/volumes?q=action')
		jsonresponse3 = urllib.request.urlopen('https://www.googleapis.com/books/v1/volumes?q=comic')
		jsonresponse4 = urllib.request.urlopen('https://www.googleapis.com/books/v1/volumes?q=design')

		self.assertEqual(jsonresponse1.status, 200)
		self.assertEqual(jsonresponse2.status, 200)
		self.assertEqual(jsonresponse3.status, 200)
		self.assertEqual(jsonresponse4.status, 200)