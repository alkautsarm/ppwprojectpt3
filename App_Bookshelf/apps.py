from django.apps import AppConfig


class AppBookshelfConfig(AppConfig):
    name = 'App_Bookshelf'
