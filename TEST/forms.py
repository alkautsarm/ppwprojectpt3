from django import forms

class TestForm(forms.Form) :
	emailForm = forms.CharField(label="", max_length=100,
		widget = forms.TextInput(attrs={'placeholder':'Phone, email or username', 'class':'form-control'}))
	passwordForm = forms.CharField(label="", min_length=8, max_length=35,
		widget = forms.PasswordInput(attrs={'placeholder':'Password', 'class':'form-control'}))
