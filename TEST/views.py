from django.shortcuts import render
from .forms import TestForm
from .models import TestModels

# Create your views here.
def test(request):
	response = {'form' : TestForm}
	testForm = TestForm(request.POST or None)

	if(request.method == 'POST' and testForm.is_valid()) :
		email_user = request.POST['emailForm']
		password_user = request.POST['passwordForm']

		testModel = TestModels(email = email_user, password = password_user)
		testModel.save()
		response = {}
		return render(request, 'test2.html', response)

	return render(request, 'test.html', response)