from django.test import TestCase

# Create your tests here.
class LoginPageUnitTest(TestCase) :

	def test_login_page_exist(self) :
		response = self.client.get('/login')
		self.assertEqual(response.status_code, 200)
	
	def test_login_page_using_templates(self) :
		response = self.client.get('/login')
		self.assertTemplateUsed(response, 'app_login_page.html')

	def test_logout_page_exist(self) :
		response = self.client.get('/logout')
		self.assertEqual(response.status_code, 200)
	
	def test_login_page_using_templates(self) :
		response = self.client.get('/logout')
		self.assertTemplateUsed(response, 'app_logout_page.html')
