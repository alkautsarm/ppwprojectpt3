from django.urls import path, include
from . import views

urlpatterns = [
	path('login', views.loginPage, name='login_page'),
	path('logout', views.logoutPage, name='logout_page'),
]