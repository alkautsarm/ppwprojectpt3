from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import logout

# Create your views here.
def loginPage(request) :
	response = {}
	return render(request, 'app_login_page.html', response)

def logoutPage(request) :
	response = {}

	if(request.user.is_authenticated) :
		logout(request)

	return render(request, 'app_logout_page.html', response)
